# -*- coding: utf-8 -*-

import os
import re
import shutil
import markdown
from jinja2 import Environment, FileSystemLoader


# Single function for copying directory trees or single files.
# Asks forgiveness instead of permission. Apparently that's what
# you're supposed to do.
def copy(src, dst):
    if os.path.isdir(src):
        try:
            shutil.copytree(src, dst)
        except:
            pass
    else:
        shutil.copy(src, dst)


# Copy a sequence of things, which can be directories or files.
def copy_things(sources, in_dir, out_dir):
    for s in sources:
        if type(s) in [tuple, list]:
            s = s[0]
        print('copy: ', s)
        copy(in_dir+s, out_dir+s)


# Process a list of templates, with a global list of template
# variables to apply to them.
def process_templates(templates, in_dir, out_dir, template_vars={}):
    env = Environment(loader=FileSystemLoader(in_dir))
    for t_dir in templates:
        if not os.path.isdir(os.path.join(out_dir, t_dir)):
            print('create: ', t_dir)
            os.makedirs(os.path.join(out_dir, t_dir))

        for t_file in templates[t_dir]:
            t_file = os.path.join(t_dir, t_file)
            print('gen:', t_file)
            template = env.get_template(t_file)
            rendered = template.render(**template_vars)
            with open(os.path.join(out_dir, t_file), 'w') as f:
                f.write(rendered)


# Process a dictionary of sequentials into the configured pathnames.
def process_sequentials(sequentials, in_dir, out_dir, template_vars={}):
    env = Environment(loader=FileSystemLoader(in_dir))
    for s_root in sequentials:
        if not os.path.isdir(os.path.join(out_dir, s_root)):
            print('create: ', s_root)
            os.makedirs(os.path.join(out_dir, s_root))

        base = s_root['base']
        pattern = s_root['pattern']
        t = os.path.join(s_root['base'], s_root['template'])
        template = env.get_template(t)
        t_files = os.listdir(base)
        replace_ele = [m.start for m in re.finditer('\%', pattern)]

        for t_file in t_files:
            t_parts = os.path.splitext(t_file)[0].split('-')
            t_file = os.path.join(in_dir, base, t_file)
            t_outpath = pattern
            for ele in replace_ele:
                n = int(pattern[ele+1])
                np = '%'+pattern[ele+1]
                if pattern[ele+2] == ':':
                    t_outpath = t_outpath.replace(np+':', t_parts[n:])
                else:
                    t_outpath = t_outpath.replace(np, t_parts[n])
            t_outdir = os.path.dirname(t_outpath)
            if not os.path.isdir(os.path.join(out_dir, s_root, t_outdir)):
                print('create: {}/{}'.format(s_root, t_outdir))
                os.makedirs(os.path.join(out_dir, s_root, t_outdir))
            print('gen:', t_outpath)
            with open(t_file) as f:
                seq_vars = template_vars.copy()
                lines = f.readlines()
                seq_vars['title'] = lines[0]
                seq_vars['content'] = markdown.markdown(''.join(lines[2:]))
            rendered = template.render(**seq_vars)
            with open(os.path.join(out_dir, t_outpath), 'w') as f:
                f.write(rendered)


# Applies a practically infinite number of configured filters
# to files which have them specified.
def apply_filters(files, out_dir):
    for f in files:
        if type(f) in [tuple, list]:
            file_name = out_dir + f[0]
            print('filter: ', file_name)
            filters = f[1:]
            file_data = open(file_name).read()
            data_len = len(file_data)
            t = 0
            for filter in filters:
                print(' ->', repr(filter).split(' ')[1], end='')
                file_data = filter(file_data)
                if len(file_data) < data_len:
                    diff = data_len - len(file_data)
                    print(' (-{} bytes)'.format(diff))
                    data_len = len(file_data)
                    t += diff
            print(' !! {} bytes saved'.format(t))
            open(file_name, 'w').write(file_data)


def gen(in_dir, out_dir, remove_output=False, copy_dirs=[], copy_files=[],
        templates={}, template_vars={}, sequentials={}, filters=()):
            if remove_output:
                for root, dirs, files in os.walk(out_dir):
                    for f in files:
                        target = os.path.join(root, f)
                        if '.git' not in target:
                            os.unlink(target)
                    for d in dirs:
                        target = os.path.join(root, d)
                        if '.git' not in target:
                            shutil.rmtree(target)

            copy_things(copy_dirs, in_dir, out_dir)
            copy_things(copy_files, in_dir, out_dir)
            process_templates(templates, in_dir, out_dir, template_vars)
            process_sequentials(sequentials, in_dir, out_dir, template_vars)
            apply_filters(filters, out_dir)
