## What is this thing?

`sitegen` is a stupidly simple Jinja2-based static site generator. Unlike most static site generators, this one offers
no fancy features. It just takes in Jinja2 templates and spits out HTML. I wrote it to manage a small static site I
use and found it tedious to learn and setup other static site generators.

If you need really advanced features, `sitegen` is not for you. If you need nothing more than to just insert templates
and receive HTML, and copy your static files to a different directory, you've come to the right place. I'm not even
joking, this thing doesn't do anything else at all.

## First things first...

There are bugs. Unicode doesn't work properly, and sometimes creating the output directory throws an exception.
If you want to fix them, please feel free to do so and I will merge your changes. I just wanted to bang this thing out
real quick so I could manage a small static site, and I didn't need Unicode nor did I mind having to manually create
a directory sometimes.

## So how do I use it?

Just create a `config.py` with your desired settings. The included example shows every available option. They are all
required because I didn't feel like adding a bunch of error checking (like I said, I wanted this thing *right now*),
but some can, of course, be blank.

Now you write your templates and stick them wherever you want in your `config.INPUT_DIRECTORY`. If you're familiar with
Jinja2 or Django, everything works just like that. There should be no surprises here. Be sure to update `config.TEMPLATES`
when you add or remove a template.

~~**Be forewarned that this config file is exec'd, not imported. This is so you can put your config file anywhere you want.**~~

Not anymore!

The config file is now just a script you write. You pass the required variables to a generator function in `sitegen`.

Then you simply run your `generate.py` (or whatever filename you give it). Done and done. Upload away.

## Filters

`sitegen` allows you to apply an arbitrary number of filters to any file in your output tree by specifying the copy file as
a list or tuple, the head element being the file and the tail elements being your funtions.

Examples:

```python
import jsmin

FILTERS = (
	('assets/js/gh.js', jsmin.jsmin),
)
```

```python
def all_caps(txt):
	return txt.upper()

def underscores_to_dashes(txt):
	return txt.replace('_', '-')

FILTERS = (
	('something.html', all_caps, underscores_to_dashes),
)
```

That last one is a little contrived, but you get the idea.

## Sequentials

Sequential content generation is completely untested, so you might not want to use it just yet.