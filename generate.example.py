# -*- coding: utf-8 -*-

# Import sitegen (duh)
import sitegen

# Import functions to use as filters if you like
from slimit import minify

# Take a wild guess.
INPUT_DIRECTORY = 'input/'
OUTPUT_DIRECTORY = 'output/'

# Remove output directory before generating?
REMOVE_OUTPUT = False

# Directories to be copied straight from INPUT_DIRECTORY
# to OUTPUT_DIRECTORY.
COPY_DIRS = (
    'css/',
    'js/',
    'img/'
)

# Same as above, but files.
COPY_FILES = (
    'favicon.ico',
    ('js/main.js', minify), # apply filters to a file
)

# A dictionary of tuples of all templates, keyed by
# directory.
TEMPLATES = {
    '': (
        'index.htm',
        'about.htm',
    ),
    'people/': (
        'ceo.htm',
        'intern.htm',
        'secretary.htm',
        'janitor.htm',
        'office-jester.htm',
    )
}

# Template variables used by any or all of your templates. Best
# used by only your layout, but you can use them for whatever
# you want.
TEMPLATE_VARS = {
    'title': 'My Site',
    'header': 'Hello, world!',
    'css_url': 'http://mysite.com/css/',
    'js_url': 'http://mysite.com/js/',
    'img_url': 'http://mysite.com/img/',
}

# A dictionary of dictionaries describing sequential content. URLs will
# be decided by filenames written like slugs, split on the dashes. The
# numbers in the pattern refer to indexes of that list.
SEQUENTIALS = {
    'blog': {
        base: 'blog/',
        template: 'blog-template.htm',
        pattern: '%0/%1/%2/%3:.htm',
    },
}

sitegen.gen(INPUT_DIRECTORY, OUTPUT_DIRECTORY, REMOVE_OUTPUT, COPY_DIRS, COPY_FILES, TEMPLATES, TEMPLATE_VARS, SEQUENTIALS)
